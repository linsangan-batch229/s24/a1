// Exponent Operator
let getCube = 2**3;


// Template Literals
console.log(`The Cube of 2 is ${getCube}`);


// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
let [houseNumber, streetName, city, zipCode] = address;
console.log(`I live at ${houseNumber} ${streetName}, ${city} ${zipCode}`);



// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`);


// Arrow Functions
let numbers1 = [1, 2, 3, 4, 5];

numbers1.forEach((num) => {
    console.log(num);
});





// Javascript Classes
class Dogo{
    constructor(name1, age, breed){
        this.name1 = name1;
        this.age = age;
        this.breed = breed;
    }
}

let dogo1 = new Dogo("Rico", 2, "Aspin");
let dogo2 = new Dogo("Nana", 3, "Bull terrier");

console.log(dogo1);
console.log(dogo2);